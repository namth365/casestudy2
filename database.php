<?php
$db_host = 'localhost';
$db_user = 'root';
$db_pass = '';
$db_name = 'lienquan';

$connect = new PDO('mysql:host='.$db_host.';dbname='.$db_name, $db_user, $db_pass);

//lấy tất cả records
// $sql    = "SELECT * FROM sinhvien";
// $stmt  = $connect->query( $sql );
// $stmt->setFetchMode(PDO::FETCH_OBJ);
// $students = $stmt->fetchAll();

// //lấy record duy nhất dựa vào id
// $sql    = "SELECT * FROM sinhvien WHERE id = 1";

// $stmt  = $connect->query( $sql );
// $stmt->setFetchMode(PDO::FETCH_OBJ);
// $sinhvien = $stmt->fetch();

// //insert
// /*
// stdClass Object
// (
//     [id] => 1
//     [student_name] => Nguy?n V?n A
//     [class_name] => C01
//     [point] => 9.1
// )
// */

// $sql    = " INSERT INTO sinhvien 
//     ( 
//         student_name,
//         class_name,
//         point
//     ) 
//     VALUES 
//     ( 
//         'Nguyen Van CC',
//         'C01',
//         '10'
//     ) 
// ";
// //$connect->query( $sql );

// //update
// //cập nhật dữ liệu
// $sql    = " UPDATE sinhvien SET 
//             student_name = 'Nguyen Van CC1',
//             class_name = 'C01' 
//             WHERE id = 1 ";
// $connect->query( $sql );

// //delete 
// $sql	= "DELETE FROM sinhvien WHERE id = 1";
//$connect->query( $sql );
