<!doctype html>
<html lang="vi">

<head>


    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="https://lienquan.garena.vn/asset/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://lienquan.garena.vn/asset/css/fonts.css">

    <!-- Zozo Accordion css -->
    <link href="https://lienquan.garena.vn/asset/css/zozo.accordion.min.css" rel="stylesheet">
    <link href="https://lienquan.garena.vn/asset/css/nice-select.css" rel="stylesheet">
    <!-- css main-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.css" />
    <link rel="stylesheet" href="https://lienquan.garena.vn/asset/css/main.css?v=20200526">

    <link rel="stylesheet" href="https://lienquan.garena.vn/asset/css/library.css">

    <link rel="apple-touch-icon" sizes="57x57" href="/kg/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/kg/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/kg/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/kg/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/kg/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/kg/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/kg/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/kg/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/kg/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/kg/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/kg/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/kg/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/kg/favicon/favicon-16x16.png">
    <link rel="manifest" href="/kg/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/kg/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#3372b2">





    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-57S6M3T');
    </script>
    <!-- End Google Tag Manager -->

</head>

<body class="wrapper_page ">

     <!-- menu mobile -->
        <style>
            /*.fb_iframe_widget {*/

            /*float: right !important;*/
            /*}*/
            .img_18 {
                position: fixed;
                z-index: 9999;
                top: 20px;
            }

            .block-right .listnews li:first-child {
                width: 100%
            }

            .block-right .listnews li:first-child {
                padding-right: 0px;
            }

            .block-right .listnews li:nth-of-type(2) {
                padding-right: 18px;
            }

            .block-right .listnews li:nth-of-type(2) {
                padding-left: 0px;
            }

            .block-right .listnews li:nth-of-type(3) {
                padding-left: 18px;
            }

            .block-right .listnews li:nth-of-type(3) {
                padding-right: 0px;
            }

            .block-right .listnews li:last-child {
                width: auto
            }

            @media only screen and (max-width: 900px) and (min-width: 0px) {
                .tabs-fata li {
                    width: 20%;
                }
            }

            @media only screen and (max-width: 900px) and (min-width: 0px) {
                .bt-down {
                    top: 60% !important;
                }

                .img_18 {
                    position: fixed;
                    z-index: 9999;
                    top: 100px !important;
                }


            }

            @media only screen and (max-width: 480px) and (min-width: 0px) {
                .bt-down {
                    top: 75% !important;
                }
            }

            @media only screen and (max-width: 500px) and (min-width: 0px) {
                body {
                    margin-bottom: 50px !important;
                }

                .img_18 {
                    width: 40px;
                    top: 110px !important;
                }
            }

            @media only screen and (min-width: 0px) and (max-width: 768px) {



                .block-right .listnews li:nth-of-type(2) {
                    padding-right: 10px !important;
                }

                .block-right .listnews li:nth-of-type(2) {
                    padding-left: 0px !important;
                }

                .block-right .listnews li:nth-of-type(3) {
                    padding-left: 10px !important;
                }

                .block-right .listnews li:nth-of-type(1) {
                    padding-right: 0px !important;
                }

                .block-right .listnews li:nth-of-type(3) {
                    padding-right: 0px !important;
                }

                .block-right .listnews li:last-child {
                    width: 50%
                }
            }
        </style>
        <img src="" class="img_18" alt="">
        <section class="menu-m">
        
        </section>
        <header class="head-top">
            <div class="top-pc">
                <nav class="nav-top">
              
                </nav>
            </div>
            <!-- top mobile -->
            <div class="top-m">


                <div class="nav-m" style="position: fixed; z-index: 99; width: 100%">
                    <div class=" avatargame">
                        <div class="avatar"><img src="https://cdn.vn.garenanow.com/web/kg/home/images/app-icon-new.png" alt="" /></div>
                        <div class="textinfo">
                            <h2>GARENA LIÊN QUÂN MOBILE</h2>
                            <p style="overflow: visible">THẮNG BẠI TẠI KỸ NĂNG</p>
                        </div>
                    </div>

                    <a href="https://app.appsflyer.com/com.garena.game.kgvn?pid=OrganicA&amp;c=obt_lp_and" onclick="goog_report_conversion('https://app.appsflyer.com/com.garena.game.kgvn?pid=OrganicA&c=obt_lp_and'); fbq('trackCustom', 'cbt_lp_apk'); var that=this;ga('send', 'event', 'home_Banner',',apk_download',',button');" class="btdown mb"><img src="https://lienquan.garena.vn/asset/images/downgame.png" alt="" /></a>

                    <!-- <a class="mobile-banner" target="_blank" href="http://draw.re/hd-nap-the-garena"> -->
                    <!-- <img src="/asset/images/mobile-card.jpg"> -->
                    <!-- </a> -->
                    <!-- <a class="mobile-banner" target="_blank" href="http://pesc.pw/nap-quan-huy-sms-viettel">
              <img src="/asset/images/banners/LQ-napthe-viettel9029-napthe-449x63.jpg">
        </a> -->
                    <!-- <a class="mobile-banner" target="_blank" href="https://lienquan.garena.vn/tin-tuc/huong-dan-mua-the-garena-nap-quan-huy-lien-quan-nhanh-tien-nhat">
            <img src="/asset/images/banners/449x63.jpg?1">
        </a> -->
                </div>

                <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title" style="color: black; ">Hướng dẫn tải game trên android</h4>
                            </div>
                            <div class="modal-body">
                                <img src="/asset/images/apk-popup-fb.png" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <!--SLIDER BANNER-->
            <div class="banner-top" style="padding-top: 60px">
                <div class="item active" style="padding-top: 60px">
                    <a href="" target="_blank">
                        <picture>
                            <source srcset="https://lienquan.garena.vn/files/upload/images/KV-SSM-32-750x400.jpg" media="(max-width: 700px)">
                            </source>
                            <source srcset="https://lienquan.garena.vn/files/upload/images/KV-SSM-32-1920x449.jpg">
                            </source>
                            <img src="https://lienquan.garena.vn/files/upload/images/KV-SSM-32-1920x449.jpg" alt="img-top">
                        </picture>
                    </a>
                </div>

                <!-- <div class="banner-top" style="padding-top: 60px">
    <a href="https://youtu.be/8_FCoUI_fOM" target="_blank">
        <picture>
        <source srcset="https://cdn.vn.garenanow.com/web/kg/banner/poster-nghe-si-2-1920x1080.jpg" media="(max-width: 780px)"> </source>
        <source srcset="https://cdn.vn.garenanow.com/web/kg/banner/poster-nghe-si-2-1920x1080.jpg"> </source>
        <img src="https://cdn.vn.garenanow.com/web/kg/banner/poster-nghe-si-2-1920x1080.jpg" alt="img-top">
        </picture>
    </a>
</div> -->
                <div class="info-library">
                    <h2 class="title"><strong>Học viện</strong> liên quân</h2>
                    <p>Chào mừng kiện tướng đến với <strong>HỌC VIỆN LIÊN QUÂN</strong>. Tại đây kiện tướng có thể tìm kiếm đầy đủ thông tin về Liên Quân Mobile, kéo xuống dưới để tìm hiểu thông tin chi tiết </p>
                </div>
        </header>