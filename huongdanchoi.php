<?php include 'layout/header.php'; ?>
<?php include 'layout/menu.php'; ?>

<section class="mainCont">
    <div class="tab-link info-tab">
        <ul class="nav-link">
            <li class=""><a href="https://localhost/server_time/weblienquan_Test/gioithieu.php">Giới thiệu</a></li>
            <li class=""><a href="https://localhost/server_time/weblienquan_Test/tuong.php">Tướng</a></li>
            <li class=""><a href="https://localhost/server_time/weblienquan_Test/trang-bi.php">Trang bị</a></li>
            <li class=""><a href="https://localhost/server_time/weblienquan_Test/bxh.php">Xếp Hạng</a></li>
            <li class="current"><a href="https://localhost/server_time/weblienquan_Test/huongdanchoi.php">Hướng Dẫn</a></li>
        </ul>
    </div> <!-- Tướng -->
    <?php include 'database.php'; ?>
    <?php

    $tam_danh = '';
    if (isset($_GET['tam_danh'])) {
        $tam_danh = $_GET['tam_danh'];
    }
    $nhom = '';
    if (isset($_GET['nhom'])) {
        $nhom = $_GET['nhom'];
    }
    $sat_thuong = '';
    if (isset($_GET['sat_thuong'])) {
        $sat_thuong = $_GET['sat_thuong'];
    }
    $phe_phai = '';
    if (isset($_GET['phe_phai'])) {
        $phe_phai = $_GET['phe_phai'];
    }

    $sql = "SELECT * FROM tuong WHERE id > 0 ";
    if ($tam_danh) {
        $sql .= " AND tamdanh ='$tam_danh' ";
    }
    if ($nhom) {
        $sql .= " AND nhom ='$nhom' ";
    }
    if ($sat_thuong) {
        $sql .= " AND satthuong ='$sat_thuong' ";
    }
    if ($phe_phai) {
        $sql .= " AND phephai ='$phe_phai' ";
    } else {
    }
    // echo '<pre>';
    // print_r($sql);
    // echo '</pre>';

    $stmt = $connect->query($sql);
    $stmt->setFetchMode(PDO::FETCH_OBJ);
    $tuongs = $stmt->fetchAll();

    ?>
</section>

<section class="heroes-page">

    <div class="inner-page">

        <!-- Đóng khung trang web  -->
        <div class="content">
            <div class="breadLine">
                <div class="head">
                    <div class="isw-grid"></div>
                    <h1 style="color:blueviolet"> LỰA CHỌN PHONG CÁCH CHƠI ĐỂ TÌM RA VỊ TƯỚNG PHÙ HỢP</h1>

                </div>

            </div>

            <div class="workplace">
                <div class="row-fluid">

                    <div class="span12">

                        <div class="block-fluid">
                            <form method="GET" action="">
                                <div class="container">
                                    <div class="tab-link info-tab">

                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="row-form">
                                                    <div class="span3"> . 
                                                        <div class="span9">
                                                            <p>
                                                                <select name="tam_danh">
                                                                    <option value="">-TẦM ĐÁNH-</option>
                                                                    <option id="filter-1" type="radio" value="Xa" class="radio">Xa</option>
                                                                    <option id="filter-1" type="radio" value="Gần" class="radio">Gần</option>
                                                                </select>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="row-form">
                                                    <div class="span3">.</div>
                                                    <div class="span9">
                                                        <p>
                                                            <select name="nhom">
                                                                <option value="">-VỊ TRÍ-</option>
                                                                <option id="filter-2" type="radio" value="Đấu Sĩ" class="radio">Đấu Sĩ</option>
                                                                <option id="filter-2" type="radio" value="Pháp Sư" class="radio">Pháp Sư</option>
                                                                <option id="filter-2" type="radio" value="Xạ Thủ" class="radio">Xạ Thủ</option>
                                                                <option id="filter-2" type="radio" value="Sát Thủ" class="radio">Sát Thủ</option>
                                                                <option id="filter-2" type="radio" value="Đỡ Đòn" class="radio">Đỡ Đòn</option>
                                                                <option id="filter-2" type="radio" value="Trợ Thủ" class="radio">Trợ Thủ</option>
                                                            </select>
                                                        </p>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="row-form">
                                                    <div class="span3">.</div>
                                                    <div class="span9">
                                                        <p>
                                                            <select name="sat_thuong">
                                                                <option value="">-SÁT THƯƠNG-</option>
                                                                <option id="filter-3" type="radio" value="Phép" class="radio">Phép</option>
                                                                <option id="filter-3" type="radio" value="Vật Lý" class="radio">Vật Lý</option>
                                                            </select>
                                                        </p>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="row-form">
                                                    <div class="span3">.</div>
                                                    <div class="span9">
                                                        <p>
                                                            <select name="phe_phai">
                                                                <option value="">-PHE PHÁI-</option>
                                                                <option id="filter-4" type="radio" value="Norman" class="radio">Lâu Đài Khởi Nguyên</option>
                                                                <option id="filter-4" type="radio" value="Veda" class="radio">Cung Điện Ánh Sáng</option>
                                                                <option id="filter-4" type="radio" value="Afata" class="radio">Khu Rừng Chạng Vạng</option>
                                                                <option id="filter-4" type="radio" value="Lokheim" class="radio">Lực Lượng Sa Đọa</option>

                                                            </select>
                                                        </p>
                                                    </div>

                                                    <div class="clear"></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>




                                </div>



                        </div>
                        <div class="tab-link info-tab">
                            <div class="row-form">
                                <button class="btn btn-success" id="filter-name" type="submit">Lựa Chọn</button>
                            </div>
                            <div class="clear"></div>

                        </div>







                        <div class="bxlisthero">
                            <ul class="listhero">
                                <?php foreach ($tuongs as $tuong) : ?>
                                    <li id="champion-1" class="list-champion">
                                        <div class="heroes"> <a href="chitiettuong.php?id_tuong=<?= $tuong->id ?>">
                                                <img src="<?= "img/users/" . $tuong->anh; ?>" alt="" />
                                            </a>
                                            <p style="white-space: nowrap; text-align:center" data-id="1" data-type="6" data-name="" class="name"><?= $tuong->tentuong ?></p>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="dr"><span></span></div>
                        <?php include 'layout/footer.php'; ?>
                    </div>
</section>
</div>