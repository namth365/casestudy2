<?php include 'database.php'; ?>
<?php
$id = $_GET['id'];
$sql    = "SELECT * FROM trangbi WHERE id = " . $id;
$stmt  = $connect->query($sql);
$stmt->setFetchMode(PDO::FETCH_OBJ);
$trangbi = $stmt->fetch();
?>

<?php
$trangbis = '';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $id             = $_REQUEST['id'];
    $matrangbi = $_REQUEST['matrangbi'];
    $tentrangbi = $_REQUEST['tentrangbi'];
    $phuhop = $_REQUEST['phuhop'];
    $nhom = $_REQUEST['nhom'];
    $noitai = $_REQUEST['noitai'];
    $anh = $_FILES['anh']['name'];

    $tmp_img = $_FILES['anh']['tmp_name'];
    $div = explode('.', $anh);
    $file_ext = strtolower(end($div));
    $unique_image = time() . '.' . $file_ext;
    $path_uploads = "./../img/tbi/".$unique_image;
    move_uploaded_file($tmp_img, $path_uploads);

    $sql    = " UPDATE trangbi SET 
		            matrangbi 	= '$matrangbi',
		            tentrangbi 		= '$tentrangbi',
                    phuhop = '$phuhop',
                    nhom = '$nhom',
                    noitai = '$noitai',
                    anh = '$unique_image'
		            WHERE id = " . $id;
    $connect->query($sql);
    header("Location: trangbi.php");
}

?>

<!-- VIEW -->
<?php include 'layout/header.php'; ?>
<?php include 'layout/menu.php'; ?>
<div class="content">


    <div class="breadLine">

        <ul class="breadcrumb">
            <li><a href="list-products.html">Danh sách</a> <span class="divider">></span></li>
            <li class="active"></li>
        </ul>

    </div>

    <div class="workplace">

        <div class="row-fluid">

            <div class="span12">

                <?php if (isset($_SESSION['trangbi'])) : ?>
                    <p id="trangbi" style="color: green;">
                        <?= $_SESSION['trangbi']; ?>
                    </p>

                <?php endif; ?>





                <div class="head">
                    <div class="isw-grid"></div>
                    <h1>Quản Lý Tướng</h1>

                    <div class="clear"></div>
                </div>
                <div class="block-fluid">
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="row-form">
                            <div class="row-form">
                                <div class="span3">Mã Trang Bị:</div>
                                <div class="span9"><input type="text" placeholder="Mã Trang Bị" name="matrangbi" value="<?php echo $trangbi->matrangbi; ?>" /></div>
                                <div class="clear"></div>
                            </div>
                            <div class="row-form">
                                <div class="span3">Tên Trang Bị:</div>
                                <div class="span9"><input type="text" placeholder="Tên Trang Bị" name="tentrangbi" value="<?php echo $trangbi->tentrangbi; ?>" /></div>
                                <div class="clear"></div>
                            </div>
                            <div class="row-form">
                                <div class="span3">Phù Hợp:</div>
                                <div class="span9"><input type="text" placeholder="Phù Hợp" name="phuhop" value="<?php echo $trangbi->phuhop; ?>" /></div>
                                <div class="clear"></div>
                            </div>
                            <div class="row-form">
                                <div class="span3">Nhóm:</div>
                                <div class="span9"><input type="text" placeholder="Nhóm" name="nhom" value="<?php echo $trangbi->nhom; ?>" /></div>
                                <div class="clear"></div>
                            </div>
                            <div class="row-form">
                                <div class="span3">Nội Tại:</div>
                                <div class="span9"><input type="text" placeholder="Nội Tại" name="noitai" value="<?php echo $trangbi->noitai; ?>" /></div>
                                <div class="clear"></div>
                            </div>
                            <div class="row-form">
                                <div class="span3">Ảnh:</div>
                                <div class="span9">
                                    <img src="" />
                                    <br />
                                    <input type="file" name="anh">
                                </div>
                                <div class="clear"></div>
                            </div>

                        </div>
                        <div class="row-form">
                            <button class="btn btn-success" type="submit">Cập Nhật</button>
                            <a class="btn btn-danger" href="trangbi.php">Thoát</a>
                            <div class="clear"></div>
                        </div>
                    </form>
                    <div class="clear"></div>
                </div>
            </div>

        </div>
        <div class="dr"><span></span></div>

    </div>
    <?php if (isset($_SESSION['trangbi'])) : ?>
        <script type="text/javascript">
            setTimeout(function() {
                document.getElementById('trangbi').remove();
            }, 2000);
        </script>
    <?php endif; ?>
    <?php include 'layout/footer.php'; ?>