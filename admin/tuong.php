<?php include 'database.php'; ?>
<?php
//lấy tất cả records
$sql    = "SELECT * FROM tuong";
$stmt  = $connect->query($sql);
$stmt->setFetchMode(PDO::FETCH_OBJ);
$tuongs = $stmt->fetchAll();
?>

<?php include 'layout/header.php'; ?>
<?php include 'layout/menu.php'; ?>

<div class="content">


    <div class="breadLine">

        <ul class="breadcrumb">
            <li><a href="list-users.html"> </a></li>
        </ul>

    </div>

    <div class="workplace">

        <div class="row-fluid">
            <div class="span12 search">
                <form>
                    <input type="text" class="span11" placeholder="Tên tướng..." name="search" />
                    <button class="btn span1" type="submit">Tìm kiếm</button>
                </form>
            </div>
        </div>

        <div class="row-fluid">

            <div class="span12">
                <div class="head">
                    <div class="isw-grid"></div>
                    <h1>Hệ Thống Tướng</h1>

                    <div class="clear"></div>
                </div>
                <div class="block-fluid table-sorting">
                    <a href="add-tuong.php" class="btn btn-add">Thêm</a>
                    <table cellpadding="0" cellspacing="0" width="100%" class="table" id="tSortable_2">
                        <thead>
                            <tr>
                                <th width="4%" class="sorting"><a href="#">ID</a></th>
                                <th width="10%" class="sorting"><a href="#">Mã Tướng</a></th>
                                <th width="10%" class="sorting"><a href="#">Tên Tướng</a></th>
                                <th width="10%" class="sorting"><a href="#">Vị Trí</a></th>
                                <th width="10%" class="sorting"><a href="#">Sát Thương</a></th>
                                <th width="7%" class="sorting"><a href="#">Tầm Đánh</a></th>
                                <th width="9%" class="sorting"><a href="#">Nhóm</a></th>
                                <th width="9%" class="sorting"><a href="#">Phe Phái</a></th>
                                <th width="10%" class="sorting"><a href="#">Lối Chơi</a></th>
                                <th width="6%" class="sorting"><a href="#">Giới</a></th>
                                <th width="15%" class="sorting"><a href="#">Ảnh</a></th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($tuongs as $tuong) : ?>
                                <tr>
                                    <td><?= $tuong->id; ?></td>
                                    <td><?= $tuong->matuong; ?></td>
                                    <td><?= $tuong->tentuong; ?></td>
                                    <td><?= $tuong->vitri; ?></td>
                                    <td><?= $tuong->satthuong; ?></td>
                                    <td><?= $tuong->tamdanh; ?></td>
                                    <td><?= $tuong->nhom; ?></td>
                                    <td><?= $tuong->phephai; ?></td>
                                    <td><?= $tuong->loichoi; ?></td>
                                    <td><?= $tuong->gioi; ?></td>
                                    <td><img width="150" src="<?= "./../img/users/".$tuong->anh; ?>"> </td>
                                    <td>
                                        <a href="sua-tuong.php?id=<?= $tuong->id; ?>" class="btn btn-info">Sửa</a>
                                    </td>
                                    <td>
                                        <a href="xoa-tuong.php?id=<?= $tuong->id; ?>" class="btn btn-danger" onclick = "return confirm('XÓA TƯỚNG ?')">Xóa</a>
                                    </td>

                                </tr>
                            <?php endforeach; ?>

                        </tbody>
                    </table>
                    <div class="dataTables_paginate">
                        <a class="first paginate_button paginate_button_disabled" href="#">Trước</a>
                        <a class="previous paginate_button paginate_button_disabled" href="#">Lùi</a>
                        <span>
                            <a class="paginate_active" href="#">1</a>
                            <a class="paginate_button" href="#">2</a>
                        </span>
                        <a class="next paginate_button" href="#">Tiếp</a>
                        <a class="last paginate_button" href="#">Sau</a>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

        </div>
        <div class="dr"><span></span></div>

    </div>

</div>
<?php include 'layout/footer.php'; ?>