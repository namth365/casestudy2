<?php
include_once 'database.php';

if(  $_SERVER['REQUEST_METHOD']  == 'POST'){
    $matrangbi = $_REQUEST['matrangbi'];
    $tentrangbi = $_REQUEST['tentrangbi'];
    $phuhop = $_REQUEST['phuhop'];
    $nhom = $_REQUEST['nhom'];
    $noitai = $_REQUEST['noitai'];
    $anh = $_FILES['anh']['name'];

    $tmp_img = $_FILES['anh']['tmp_name'];
    $div = explode('.',$anh);
    $file_ext = strtolower(end($div));
    $unique_image= time().'.'.$file_ext;
    $path_uploads="./../img/tbi/".$unique_image;
    move_uploaded_file($tmp_img,$path_uploads);

    $sql    = " INSERT INTO trangbi 
        ( id,
            matrangbi,
            tentrangbi,
            phuhop,
            nhom,
            noitai,
            anh,
            id_tuong
        ) 
        VALUES 
        ( NULL,
            '$matrangbi ',
            '$tentrangbi',
            '$phuhop',
            '$nhom',
            '$noitai',
            '$unique_image',
            '1'
        ) 
    ";

$connect->query($sql);
header('Location: trangbi.php');
}

?>

<?php include 'layout/header.php' ; ?>
<?php include 'layout/menu.php' ; ?>
<div class="content">


    <div class="breadLine">

        <ul class="breadcrumb">
            <li><a href="list-products.html">Danh sách</a> <span class="divider">></span></li>
            <li class="active"></li>
        </ul>

    </div>

    <div class="workplace">

        <div class="row-fluid">

            <div class="span12">
                <div class="head">
                    <div class="isw-grid"></div>
                    <h1>Quản Lý Thư Viện</h1>

                    <div class="clear"></div>
                </div>
                <div class="block-fluid">
                    <form  method="post" enctype="multipart/form-data">
                    <div class="row-form">
                            <div class="span3">Mã Trang Bị:</div>
                            <div class="span9"><input type="text" placeholder="Mã Trang Bị" name="matrangbi" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Tên Trang Bị:</div>
                            <div class="span9"><input type="text" placeholder="Tên Trang Bị" name="tentrangbi" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Phù Hợp:</div>
                            <div class="span9"><input type="text" placeholder="Phù Hợp" name="phuhop" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Nhóm:</div>
                            <div class="span9"><input type="text" placeholder="Nhóm" name="nhom" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Nội Tại:</div>
                            <div class="span9"><input type="text" placeholder="Nội Tại" name='noitai' /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Ảnh:</div>
                            <div class="span9">
                                <img src="" />
                                <br />
                                <input type="file" name="anh" size="19">
                            </div>
                            <div class="clear"></div>
                        </div>

                </div>
                <div class="row-form">
                    <button class="btn btn-success" type="submit">Cập Nhật</button>
                    <div class="clear"></div>
                </div>
                </form>
                <div class="clear"></div>
            </div>
        </div>

    </div>
    <div class="dr"><span></span></div>

</div>
<?php include 'layout/footer.php' ; ?>