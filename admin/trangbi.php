<?php include 'database.php'; ?>
<?php
//lấy tất cả records
$sql    = "SELECT * FROM trangbi";
$stmt  = $connect->query( $sql );
$stmt->setFetchMode(PDO::FETCH_OBJ);
$trangbis = $stmt->fetchAll();
// echo '<pre>' ;
// print_r ($trangbis) ;
// echo '</pre>';
?>
<?php include 'layout/header.php' ; ?>
<?php include 'layout/menu.php' ; ?>
<div class="content">


    <div class="breadLine">

        <ul class="breadcrumb">
            <li><a href="list-users.html"></a></li>
        </ul>

    </div>

    <div class="workplace">

        <div class="row-fluid">
            <div class="span12 search">
                <form>
                    <input type="text" class="span11" placeholder="Tên trang bị..." name="search"/>
                    <button class="btn span1" type="submit">Tìm kiếm</button>
                </form>
            </div>
        </div>
        <!-- /row-fluid-->

        <div class="row-fluid">

            <div class="span12">
                <div class="head">
                    <div class="isw-grid"></div>
                    <h1>Danh Mục Trang Bị</h1>

                    <div class="clear"></div>
                </div>
                <div class="block-fluid table-sorting">
                    <a href="add-trangbi.php" class="btn btn-add">Thêm</a>
                    <table cellpadding="0" cellspacing="0" width="100%" class="table" id="tSortable_2">
                        <thead>
                        <tr>
             
                            <th width="5%" class="sorting"><a href="#">ID</a></th>
                            <th width="15%" class="sorting"><a href="#">Mã Trang Bị</a></th>
                            <th width="20%" class="sorting"><a href="#">Tên Trang Bị</a></th>
                            <th width="15%" class="sorting"><a href="#">Phù Hợp</a></th>
                            <th width="15%" class="sorting"><a href="#">Nhóm</a></th>
                            <th width="20%" class="sorting"><a href="#">Nội Tại</a></th>
                            <th width="10%" class="sorting"><a href="#">Ảnh</a></th>

                                          </tr>
                        </thead>
                        <tbody>


                        <?php foreach( $trangbis as $trangbis ):?>
                        <tr>
            
                            <td><?= $trangbis->id; ?></td>
                            <td><?= $trangbis->matrangbi; ?></td>
                            <td><?= $trangbis->tentrangbi; ?></td>
                            <td><?= $trangbis->phuhop; ?></td>
                            <td><?= $trangbis->nhom; ?></td>
                           <td><?= $trangbis->noitai; ?></td>
                            <td><img width = "150" src="<?="./../img/tbi/".$trangbis->anh; ?>"> </td>
                            <td>
                                <a 
                                href="sua-trangbi.php?id=<?= $trangbis->id; ?>" 
                                class="btn btn-info">Sửa</a>
                            </td>
                            <td>
                             <a href="xoa-trangbi.php?id=<?= $trangbis->id; ?>" 
                             class="btn btn-danger"onclick = "return confirm('XÓA TRANG BỊ ?')">Xóa</a>
                        </td>
                                           </tr>
                        <?php endforeach;?>            
              
                        </tbody>
                    </table>
                 
                    <div class="dataTables_paginate">
                        <a class="first paginate_button paginate_button_disabled" href="#">Trước</a>
                        <a class="previous paginate_button paginate_button_disabled" href="#">Lùi</a>
                        <span>
                            <a class="paginate_active" href="#">1</a>
                            <a class="paginate_button" href="#">2</a>
                        </span>
                        <a class="next paginate_button" href="#">Tiếp</a>
                        <a class="last paginate_button" href="#">Sau</a>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

        </div>
        <div class="dr"><span></span></div>

    </div>

</div>

<?php include 'layout/footer.php' ; ?>
