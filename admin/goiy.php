<?php include 'database.php'; ?>
<?php

$tam_danh = '';
if (isset($_GET['tam_danh'])) {
    $tam_danh = $_GET['tam_danh'];
}
$loi_choi = '';
if (isset($_GET['loi_choi'])) {
    $loi_choi = $_GET['loi_choi'];
}
$sat_thuong = '';
if (isset($_GET['sat_thuong'])) {
    $sat_thuong = $_GET['sat_thuong'];
}
$phe_phai = '';
if (isset($_GET['phe_phai'])) {
    $phe_phai = $_GET['phe_phai'];
}

$sql = "SELECT * FROM tuong WHERE id > 0 ";
if ($tam_danh) {
    $sql .= " AND tamdanh ='$tam_danh' ";
}
if ($loi_choi) {
    $sql .= " AND loichoi ='$loi_choi' ";
}
if ($sat_thuong) {
    $sql .= " AND satthuong ='$sat_thuong' ";
}
if ($phe_phai) {
    $sql .= " AND phephai ='$phe_phai' ";
} else {
}
// echo '<pre>';
// print_r($sql);
// echo '</pre>';

$stmt = $connect->query($sql);
$stmt->setFetchMode(PDO::FETCH_OBJ);
$tuongs = $stmt->fetchAll();

?>

<?php include 'layout/header.php'; ?>
<?php include 'layout/menu.php'; ?>
<div class="content">


    <div class="breadLine">

        <ul class="breadcrumb">
            <li><a href="list-products.html"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>

    </div>

    <div class="workplace">

        <div class="row-fluid">

            <div class="span12">
                <div class="head">
                    <div class="isw-grid"></div>
                    <h1>LỰA CHỌN PHONG CÁCH CHƠI CỦA BẠN ĐỂ TÌM RA VỊ TƯỚNG PHÙ HỢP</h1>

                    <div class="clear"></div>
                </div>
                <div class="block-fluid">
                   
                <form method="GET" action="">
                        <div class="row-form">
                            <div class="span3">Tầm Đánh:</div>
                            <div class="span9">
                                <p>
                                    <select name="tam_danh">
                                        <option value="">-Tất Cả-</option>
                                        <option id="filter-1" type="radio" value="Xa" class="radio">Xa</option>
                                        <option id="filter-1" type="radio" value="Gần" class="radio">Gần</option>
                                    </select>
                                </p>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="row-form">
                            <div class="span3">Lối Chơi:</div>
                            <div class="span9">
                                <p>
                                    <select name="loi_choi">
                                        <option value="">-Tất Cả-</option>
                                        <option id="filter-2" type="radio" value="Chủ Lực" class="radio">Chủ Lực</option>
                                        <option id="filter-2" type="radio" value="Yểm Hộ" class="radio">Yểm Hộ</option>
                                    </select>
                                </p>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Sát Thương:</div>
                            <div class="span9">
                                <p>
                                    <select name="sat_thuong">
                                        <option value="">-Tất Cả-</option>
                                        <option id="filter-3" type="radio" value="Phép" class="radio">Phép</option>
                                        <option id="filter-3" type="radio" value="Vật Lý" class="radio">Vật Lý</option>
                                    </select>
                                </p>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Phe Phái:</div>
                            <div class="span9">
                                <p>
                                    <select name="phe_phai">
                                        <option value="">-Tất Cả-</option>
                                        <option id="filter-4" type="radio" value="Norman" class="radio">Norman</option>
                                        <option id="filter-4" type="radio" value="Athanor" class="radio">Athanor</option>
                                        <option id="filter-4" type="radio" value="Veda" class="radio">Veda</option>
                                        <option id="filter-4" type="radio" value="Afata" class="radio">Afata</option>
                                        <option id="filter-4" type="radio" value="Lokheim" class="radio">Lokheim</option>

                                    </select>
                                </p>
                            </div>
                            <div class="clear"></div>
                        </div>
                </div>
                <!-- 
                public function index(Request $request)
    {
        if( !$this->userCan('warehouses_index') ) $this->_show_no_access();
        $query = $this->cr_model::where('id','!=','');
     
        if( $request->search ){
            $query->where('name','LIKE','%'.$request->search.'%');
        }
        if( isset($request->filter) && count( $request->filter ) ){
            foreach( $request->filter as $field => $value ){
                if( $value ){
                    $query->where($field, 'LIKE','%'.$value.'%');
                }
            }
        }
        if( $request->sort_by ){
            switch ($request->sort_by) {
                case 'id_desc':
                    $query->orderBy('id','DESC');
                    break;
                case 'id_asc':
                    $query->orderBy('id','ASC');
                    break;
                default:
                    # code...
                    break;
            }
        }
        $warehouses = $query->paginate($this->limit);
        return view($this->cr_module.'::index',[
            'warehouses'   => $warehouses
        ]);
    }

     -->
                <div class="row-form">
                    <button class="btn btn-success" id="filter-name" type="submit">Lựa Chọn</button>
                    <div class="clear"></div>

                </div>

                    <div class="bxlisthero">
                        <ul class="listhero">
                            <?php foreach ($tuongs as $tuong) : ?>
                                <li style="list-style-type: none;" id="champion-1" class="list-champion">
                                    <div style="float:left;width:100px;height:100px;padding-right:40px;margin-bottom:50px"  class="heroes"> <a href="chitiettuong.php?id_tuong=<?= $tuong->id ?>">
                                            <img src="<?="./../img/users/".$tuong->anh; ?>" alt="" />
                                        </a>
                                        <p style="white-space: nowrap; text-align:center" data-id="1" data-type="6" data-name="" class="name"><?= $tuong->tentuong ?></p>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="dr"><span></span></div>




                    <?php include 'layout/footer.php'; ?>

                                  