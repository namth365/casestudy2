<?php
include_once 'database.php';

if(  $_SERVER['REQUEST_METHOD']  == 'POST'){
    $matuong = $_REQUEST['matuong'];
    $tentuong = $_REQUEST['tentuong'];
    $vitri = $_REQUEST['vitri'];
    $satthuong = $_REQUEST['satthuong'];
    $tamdanh = $_REQUEST['tamdanh'];
    $nhom    = $_REQUEST['nhom'];
    $phephai = $_REQUEST['phephai'];
    $loichoi = $_REQUEST['loichoi'];
    $gioi = $_REQUEST['gioi'];
    $anh = $_FILES['anh']['name'];

    $tmp_img = $_FILES['anh']['tmp_name'];
        $div = explode('.',$anh);
        $file_ext = strtolower(end($div));
        $unique_image= time().'.'.$file_ext;
        $path_uploads="./../img/users/".$unique_image;
        move_uploaded_file($tmp_img,$path_uploads);

    $sql    = " INSERT INTO tuong 
        ( id,
            matuong,
            tentuong,
            vitri,
            satthuong,
            tamdanh,
            nhom,
            phephai,
            loichoi,
            gioi,
            anh
        ) 
        VALUES 
        ( NULL,
            '$matuong',
            '$tentuong',
            '$vitri',
            '$satthuong',
            '$tamdanh',
            '$nhom',
            '$phephai',
            '$loichoi',
            '$gioi',
            '$unique_image'
                ) 
    ";
    
$connect->query($sql);
header('Location: tuong.php');
}

?>
<?php include 'layout/header.php' ; ?>
<?php include 'layout/menu.php' ; ?>
<div class="content">


    <div class="breadLine">

        <ul class="breadcrumb">
            <li><a href="list-products.html">Danh sách</a> <span class="divider">></span></li>
            <li class="active"></li>
        </ul>

    </div>

    <div class="workplace">

        <div class="row-fluid">

            <div class="span12">
                <div class="head">
                    <div class="isw-grid"></div>
                    <h1>Quản Lý Tướng</h1>

                    <div class="clear"></div>
                </div>
                <div class="block-fluid">
                    <form  method="post"  enctype="multipart/form-data">
                        <div class="row-form">
                            <div class="span3">Mã Tướng:</div>
                            <div class="span9"><input type="text" placeholder="Mã Tướng" name="matuong" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Tên Tướng:</div>
                            <div class="span9"><input type="text" placeholder="Tên Tướng" name="tentuong" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Vị Trí:</div>
                            <div class="span9"><input type="text" placeholder="Vị Trí" name="vitri" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Sát Thương:</div>
                            <div class="span9"><input type="text" placeholder="Sát Thương" name="satthuong" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Tầm Đánh:</div>
                            <div class="span9"><input type="text" placeholder="Tầm Đánh" name="tamdanh"/></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Nhóm:</div>
                            <div class="span9"><input type="text" placeholder="Nhóm" name="nhom" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Phe Phái:</div>
                            <div class="span9"><input type="text" placeholder="Phe Phái" name="phephai" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Lối Chơi:</div>
                            <div class="span9"><input type="text" placeholder="Lối Chơi" name="loichoi" /></div>
                            <div class="clear"></div>
                        </div>
                       
                        <div class="row-form">
                            <div class="span3">Giới:</div>
                            <div class="span9"><input type="text" placeholder="Giới" name="gioi" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Ảnh:</div>
                            <div class="span9">
                                <img src="" />
                                                               <input type="file" name="anh" size="19">
                            </div>
                            <div class="clear"></div>
                        </div>

                </div>
                <div class="row-form">
                    <button class="btn btn-success" type="submit">Cập Nhật</button>
                                        <button class="btn btn-success" type="submit" href ="tuong.php">Thoát</button>
                    <div class="clear"></div>
                </div>
                </form>
                <div class="clear"></div>
            </div>
        </div>

    </div>
    <div class="dr"><span></span></div>

</div>
<?php include 'layout/footer.php' ; ?>