<?php include 'database.php'; ?>
<?php
$id = $_GET['id'];
$sql    = "SELECT * FROM tuong WHERE id = " . $id;
$stmt  = $connect->query($sql);
$stmt->setFetchMode(PDO::FETCH_OBJ);
$tuong = $stmt->fetch();

// Hiển thị kết quả gọi
// echo '<pre>';
// print_r($tuong) ; 
// echo '</pre>';
// 
?>

<?php
$tuongs = '';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $id             = $_REQUEST['id'];
    $matuong     = $_REQUEST['matuong'];
    $tentuong     = $_REQUEST['tentuong'];
    $vitri            = $_REQUEST['vitri'];
    $satthuong     = $_REQUEST['satthuong'];
    $tamdanh     = $_REQUEST['tamdanh'];
    $nhom     = $_REQUEST['nhom'];
    $phephai     = $_REQUEST['phephai'];
    $loichoi     = $_REQUEST['loichoi'];
    $gioi     = $_REQUEST['gioi'];
    $anh = $_FILES['anh']['name'];

    $tmp_img = $_FILES['anh']['tmp_name'];
    $div = explode('.', $anh);
    $file_ext = strtolower(end($div));
    $unique_image = time() . '.' . $file_ext;
    $path_uploads = "./../img/users/" . $unique_image;
    move_uploaded_file($tmp_img, $path_uploads);

    // echo '<pre>';
    // print_r($_FILES);
    // echo '</pre>';
    // die();

    $sql    = " UPDATE tuong SET 
		            matuong 	= '$matuong',
		            tentuong 		= '$tentuong',
                    vitri = '$vitri',
                    satthuong = '$satthuong',
                    tamdanh = '$tamdanh',
                    nhom = '$nhom',
                    phephai = '$phephai',
                    loichoi = '$loichoi',
                    gioi = '$gioi',
                    anh = '$unique_image'
		            WHERE id = " . $id;
    $connect->query($sql);
    header("Location: tuong.php");
}

// $_SESSION['tuong'] = 'Thành công !';
// //Chuyển hướng
// header("Location: tuong.php");
?>

<!-- VIEW -->
<?php include 'layout/header.php'; ?>
<?php include 'layout/menu.php'; ?>
<div class="content">


    <div class="breadLine">

        <ul class="breadcrumb">
            <li><a href="list-products.html">Danh sách</a> <span class="divider">></span></li>
            <li class="active"></li>
        </ul>

    </div>

    <div class="workplace">

        <div class="row-fluid">

            <div class="span12">

                <?php if (isset($_SESSION['tuong'])) : ?>
                    <p id="tuong" style="color: green;">
                        <?= $_SESSION['tuong']; ?>
                    </p>

                <?php endif; ?>
                <div class="head">
                    <div class="isw-grid"></div>
                    <h1>Quản Lý Tướng</h1>

                    <div class="clear"></div>
                </div>
                <div class="block-fluid">
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="row-form">
                            <div class="span3">Mã Tướng:</div>
                            <div class="span9"><input type="text" placeholder="Mã Tướng" name="matuong" value="<?php echo $tuong->matuong; ?>" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Tên Tướng:</div>
                            <div class="span9"><input type="text" placeholder="Tên Tướng" name="tentuong" value="<?php echo $tuong->tentuong; ?>" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Vị Trí:</div>
                            <div class="span9"><input type="text" placeholder="Vị Trí" name="vitri" value="<?php echo $tuong->vitri; ?>" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Sát Thương:</div>
                            <div class="span9"><input type="text" placeholder="Sát Thương" name="satthuong" value="<?php echo $tuong->satthuong; ?>" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Tầm Đánh:</div>
                            <div class="span9"><input type="text" placeholder="Tầm Đánh" name="tamdanh" value="<?php echo $tuong->tamdanh; ?>" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Nhóm:</div>
                            <div class="span9"><input type="text" placeholder="Nhóm" name="nhom" value="<?php echo $tuong->nhom; ?>" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Phe Phái:</div>
                            <div class="span9"><input type="text" placeholder="Phe Phái" name="phephai" value="<?php echo $tuong->phephai; ?>" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Lối Chơi:</div>
                            <div class="span9"><input type="text" placeholder="Lối Chơi" name="loichoi" value="<?php echo $tuong->loichoi; ?>" /></div>
                            <div class="clear"></div>
                        </div>
                
                        <div class="row-form">
                            <div class="span3">Giới:</div>
                            <div class="span9"><input type="text" placeholder="Giới" name="gioi" value="<?php echo $tuong->gioi; ?>" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <div class="span3">Ảnh:</div>
                            <div class="span9">
                                <!-- Truyền ảnh ko có value -->
                                <img width="100" src="" />
                                <br />
                                <input type="file" name="anh">
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="row-form">
                            <button class="btn btn-success" type="submit">Cập Nhật</button>
                            <a class="btn btn-danger" href = "tuong.php">Thoát</a>
                            <div class="clear"></div>
                        </div>
                    </form>
                </div>
            <div class="clear"></div>
        </div>
    </div>

</div>
<div class="dr"><span></span></div>

</div>

</div>
<?php if (isset($_SESSION['tuong'])) : ?>
    <script type="text/javascript">
        setTimeout(function() {
            document.getElementById('tuong').remove();
        }, 2000);
    </script>
<?php endif; ?>
<?php include 'layout/footer.php'; ?>