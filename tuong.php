<?php include 'admin/database.php'; ?>
<?php

$filter = '';
if (isset($_GET['filter'])) {
    $filter = $_GET['filter'];
}
if ($filter) {
    $sql = "SELECT * FROM tuong 
   WHERE nhom ='$filter' ";
} else {
    $sql = "SELECT * FROM tuong";
}
// echo '<pre>';
// print_r ($filter);
// echo '</pre>';

$stmt = $connect->query($sql);
$stmt->setFetchMode(PDO::FETCH_OBJ);
$tuongs = $stmt->fetchAll();
// echo '<pre>';
// print_r($_GET );
// echo '</pre>';


?>

<?php include 'layout/header.php'; ?>
<?php include 'layout/menu.php'; ?>
<section class="mainCont">
    <div class="tab-link info-tab">
        <ul class="nav-link">
            <li class=""><a href="https://localhost/server_time/weblienquan_Test/gioithieu.php">Giới thiệu</a></li>
            <li class="current"><a href="https://localhost/server_time/weblienquan_Test/tuong.php">Tướng</a></li>
            <li class=""><a href="https://localhost/server_time/weblienquan_Test/trang-bi.php">Trang bị</a></li>
            <li class=""><a href="https://localhost/server_time/weblienquan_Test/bxh.php">Xếp Hạng</a></li>
            <li class=""><a href="https://localhost/server_time/weblienquan_Test/huongdanchoi.php">Hướng Dẫn</a></li>
        </ul>
    </div>

    <section class="heroes-page">
        <div class="inner-page">
            <form method="GET" action="">
                <div class="filter-hero">
                    <div class="item-filter">
                        <input id="filter-10" name="filter" type="radio" class="radio" value="" checked>
                        <label for="filter-10"><span></span> Tất cả</label>
                    </div>

                    <div class="item-filter">
                        <input id="filter-1" name="filter" type="radio" value="Đấu Sĩ" class="radio">
                        <label for="filter-1"><span></span> Đấu sĩ</label>
                    </div>


                    <div class="item-filter">
                        <input id="filter-2" name="filter" type="radio" value="Pháp Sư" class="radio">
                        <label for="filter-2"><span></span> Pháp Sư</label>
                    </div>


                    <div class="item-filter">
                        <input id="filter-3" name="filter" type="radio" value="Trợ Thủ" class="radio">
                        <label for="filter-3"><span></span> Trợ Thủ</label>
                    </div>


                    <div class="item-filter">
                        <input id="filter-4" name="filter" type="radio" value="Đỡ Đòn" class="radio">
                        <label for="filter-4"><span></span> Đỡ Đòn</label>
                    </div>

                    <div class="item-filter">
                        <input id="filter-5" name="filter" type="radio" value="Sát Thủ" class="radio">
                        <label for="filter-5"><span></span> Sát Thủ</label>
                    </div>


                    <div class="item-filter">
                        <input id="filter-6" name="filter" type="radio" value="Xạ Thủ" class="radio">
                        <label for="filter-6"><span></span> Xạ Thủ</label>
                    </div>


                </div>
                <div class="bx-search">
                    <div class="row-form">
                        <button class="btn btn-success" id="filter-name" type="submit">Lựa Chọn</button>
                        <div class="clear"></div>

                    </div>
                </div>


            </form>
            <div class="bxlisthero">
                <ul class="listhero">
                    <?php foreach ($tuongs as $tuong) : ?>
                        <li id="champion-1" class="list-champion">
                            <div class="heroes"> <a href="#">
                                    <img src="<?= "img/users/" . $tuong->anh; ?>" alt="" />
                                </a>
                                <p style="white-space: nowrap;" data-id="1" data-type="6" data-name="" class="name"><?= $tuong->tentuong ?></p>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <div class="dr"><span></span></div>
    </section>
    </div>
</section>